<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $sheep = new Animal("shaun");

    echo "Nama Hewan : ".$sheep->name."<br>";
    echo "Jumlah Kaki : ".$sheep->legs."<br>";
    echo "Berdarah dingin? : ".$sheep->cold_blooded."<br><br>";

    $kodok = new Frog("buduk");
    echo "Nama Hewan : ".$kodok->name."<br>";
    echo "Jumlah Kaki : ".$kodok->legs."<br>";
    echo "Berdarah dingin? : ".$kodok->cold_blooded."<br>";
    echo "Jump : ";
    $kodok->jump();
    echo "<br><br>";

    $sungokong = new Ape("kera sakti");
    echo "Nama Hewan : ".$sungokong->name."<br>";
    echo "Jumlah Kaki : ".$sungokong->legs."<br>";
    echo "Berdarah dingin? : ".$sungokong->cold_blooded."<br>";
    echo "Yell : ";
    $sungokong->yell();
    
    

    
?>